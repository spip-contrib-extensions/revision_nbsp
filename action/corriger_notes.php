<?php

/*
 * corrige les notes
 * Auteur : fil@rezo.net
 * (c) 2005-2006 - Distribue sous licence GNU/GPL
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function action_corriger_notes_dist($id_article = null) {
	if (is_null($id_article)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_article = $securiser_action();
	}

	if (!empty($GLOBALS['visiteur_session']['statut'])
	  and $GLOBALS['visiteur_session']['statut'] == '0minirezo') {
		include_spip('revision_nbsp');
		include_spip('base/abstract_sql');

		$article = sql_fetsel('texte', 'spip_articles', 'id_article='.intval($id_article));
		[$nb_notes, $texte] = revision_nbsp_notes_automatiques($article['texte']);
		if ($nb_notes) {
			include_spip('action/editer_objet');
			objet_modifier('article', $id_article, ['texte' => $texte]);
		}
	}
}
