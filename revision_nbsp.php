<?php

/*
 * revision_nbsp
 *
 * Dans l'espace prive, souligne en grise les espaces insecables
 *
 * Auteur : fil@rezo.net
 * © 2005-2007 - Distribue sous licence GNU/GPL
 *
 * l'icone <edit-find-replace.png> est tiree de Tango Desktop Project
 * http://tango.freedesktop.org/Tango_Desktop_Project -- sous licence
 * http://creativecommons.org/licenses/by-sa/2.5/
 *
 */


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Pipeline post_typo : mettre en évidence les nbsp
 * @param string $letexte
 * @return string
 */
function revision_nbsp_post_typo($letexte) {
	if (!empty($letexte)
	  and test_espace_prive()
	  and !defined('_STOP_REVISION_NBSP')) {
		$letexte = echappe_html($letexte, '', true, ',(<img[^<]*>),Ums');

		// NBSP classique
		$letexte = str_replace(
			'&nbsp;',
			'<span style="border-bottom:2px solid #ccc;">&nbsp;</span>',
			$letexte
		);
		// NBSP utf8
		$letexte = str_replace(
			"\xc2\xa0",
			'<span style="border-bottom:2px solid #ccc;">&nbsp;</span>',
			$letexte
		);

		// redondant avec le nouveau systeme
		$letexte = str_replace(
			"\n_ ",
			"<span style='color:orange;'>&para;</span>\n_ ",
			$letexte
		);

	}

	return $letexte;
}

/**
 * Pipeline affiche_gauche() pour ajouter le bouton de correction des notes
 * @param array $flux
 * @return array
 */
function revision_nbsp_affiche_droite($flux) {
	if (!empty($GLOBALS['visiteur_session']['statut'])
	  and $GLOBALS['visiteur_session']['statut'] == '0minirezo') {
		if (
		  $en_cours = trouver_objet_exec($flux['args']['exec'])
		  and $en_cours['type'] === 'article'
		  and $en_cours['edition'] !== true // page visu
		  and isset($flux['args']['id_article'])
		  and $id_article = intval($flux['args']['id_article'])) {

			$article =sql_fetsel("texte", "spip_articles", "id_article=".intval($id_article));
			if ($c = revision_nbsp_notes_automatiques($article['texte'])
			  and $nb_notes = reset($c)) {
				$url_action = generer_action_auteur('corriger_notes', $id_article, self());

				include_spip('inc/presentation');
				$flux['data'] .= "\n"
					. debut_boite_info(true)
					. icone_horizontale(
						'Transformer les ' . $nb_notes . ' notes de cet article.',
						$url_action,
						'find_replace-24.svg',
					)
					. fin_boite_info(true);
			}

		}

	}

	// ajouter le css des nbsp;
	$flux['data'] .= '<style type="text/css">small.fine {background-color:#ccc;}</style>';

	return $flux;
}


/**
 * Transformer les notes automatiques en fin de texte si besoin
 *
 * @param string $texte
 * @return array
 *   retourne le texte modifié si il y a eu des modifications, un texte vide sinon
 */
function revision_nbsp_notes_automatiques($texte) {
	$nb_notes = 0;

	// Attraper les notes
	$regexp = ', *\[\[(.*?)\]\],msS';
	if (
		strpos($texte, '[[') !== false
		and $s = preg_match_all($regexp, $texte, $matches, PREG_SET_ORDER)
		and $s == 1
		and preg_match(',^ *<>(.*),s', $matches[0][1], $r)
	) {
		$lesnotes = $r[1];
		$letexte = trim(str_replace($matches[0][0], '', $texte));
		// on enleve l'espace avant l'appel des notes, sinon il y en aura deux une fois la note convertie
		$letexte = preg_replace("`(?:\p{Zs}|~)(\(\d{1,2}\))`u","\\1", $letexte);

		$num = 0;
		$debut = null;
		while (
			($a = strpos($lesnotes, '(' . (++$num) . ')')) !== false
			and (
				($b = strpos($letexte, '(' . ($num) . ')')) !== false
				or ($b = strpos($letexte, '[' . ($num) . '])')) !== false
			)
		) {
			if (is_null($debut)) {
				$debut = trim(substr($lesnotes, 0, $a));
			}

			$lanote = substr($lesnotes, $a + strlen('(' . $num . ')'));

			$lanote = preg_replace(
				',[(]' . ($num + 1) . '[)].*,s',
				'',
				$lanote
			);
			$lesnotes = substr($lesnotes, $a + strlen('(' . $num . ')') + strlen($lanote));
			$lanote = trim($lanote);
			$lanote = (strlen($lanote) ? "[[\n  " . $lanote . "\n]]" : '');

			$letexte = substr($letexte, 0, $b)
				. $lanote
				. substr($letexte, $b + strlen('(' . $num . ')'));
		}



		if (strlen($suite = trim($lesnotes))) {
			$letexte .= '[[<> ' . $suite . ' ]]';
		}

		if (!is_null($debut)) {
			$texte = (strlen($debut) ? "\n\n[[<>$debut ]]" : '') . $letexte;
			$nb_notes = $num - 1;
		}
	}


	//  Cas deux : on recherche des notes en derniers paragraphes,
	// commencant par (1), on les reinjecte en [[<> ... ]] et on
	// relance la fonction sur cette construction.
	elseif (strpos($texte, '(') !== false and strpos($texte, ')') !== false) {
		$letexte = trim($texte);
		if (
			preg_match_all(',^[(](\d+)[)].*$,UmS', $letexte, $regs)
			and preg_match(',^(.*\n)([(]1[)].*)$,Us', $letexte, $u)
		) {
			$notes = $u[2];
			$letexte = $u[1];
			//var_dump($notes);
			return revision_nbsp_notes_automatiques("$letexte\n\n[[<> $notes ]]");
		}
	}

	return [$nb_notes, $texte];
}



/**
 * Transformer les notes automatiques en fin de texte si besoin
 * @deprecated
 *
 * @param string $texte
 * @return string
 *   retourne le texte modifié si il y a eu des modifications, un texte vide sinon
 */
function notes_automatiques($texte) {

	[$nb_notes, $texte] = revision_nbsp_notes_automatiques($texte);
	$GLOBALS['nb_notes'] = $nb_notes;
	return ($nb_notes ? $texte : '');
}
